# HÔM NAY MÌNH SẼ NÓI ĐẾN HÀM VÀ CÁC BẠN CÓ THỂ GỌI LÀ METHOD CŨNG CHẲNG SAO

## Hàm bắt buộc phải trả về

**Hàm này sẽ có kiểu hàm là các kiểu dữ liệu đã học `int` `double` `String` vân vân và mây mây và nó phải trả về đúng các giá trị này**

_Ở đây mình sẽ demo 1 hàm trả về `int` các hàm còn lại các bạn làm tương tự_

```
1   public class MyClass {
2     int myMethod(int a) {
3       return a;
4     }
5
6     public static void main(String[] args) {
7       System.out.print(myMethod(100));
8     }
9   }
```

Dòng 2 là dòng bắt đầu khai báo 1 hàm với `int` là kiểu hàm `myMethod` là tên hàm. Tương tự với các kiểu dữ liệu khác sẽ thay `int` bằng `String` hay `double` vân vân và mây mây

Cũng tại dòng 2 chúng ta có cụm `(int a)` với `int a` là tham số đầu vào của hàm (không bắt buộc) và nó có thể nhận bất kỳ kiểu dữ liệu nào ( ở đây mình lấy luôn `int` cho tiện). Nghĩa là hàm này sẽ nhận vào 1 tham số có tên là `a` kiểu là `int`.

Ở dòng 3 là dòng trả về giá trị của hàm bằng cách sử dụng `return`. Kiểu hàm mà bạn khai báo ở dòng 2 là gì thì ở đây `return` cũng phải trả về kiểu giá trị đó. Và điều này là bắt buộc. Tương tự với các hàm có kiểu `String` hay `double` thì cũng vậy.

Từ dòng 6 đến dòng 8 các bạn sẽ không lạ nữa (ai chưa biết có thể đọc lại **[Bài 1](https://gitlab.com/nqtho2306/learn_java/-/blob/master/Java/bien_kieudulieu_nhapxuat.md)**)

Hàm đã tạo và tạo thì tạo nhưng nó vẫn chưa thể chạy và nếu muốn nó chạy chúng ta phải gọi lại nó (dòng 7) và truyền vào cho nó 1 tham số như lúc chúng ta khai báo. Kiểu dữ liệu nào thì chúng ta truyền vào kiểu đó ở trên mình chọn tham số là `int` thì ở đây mình truyền cho nó giá trị là 100. Điều này có nghĩa tham số `a` lúc này có giá trị 100.

Và kết quả in ra màn hình ở bài trên không gì khác ngoài con số 100.

## Hàm không bắt buộc phải trả về

**Ở hàm này chúng ta sẽ không phải khai báo kiểu hàm mà thay vào đó chúng ta sẽ khai báo 1 từ khóa là `void` khi khởi tạo hàm**

```
1   public class MyClass {
2     static void myMethod() {
3       System.out.println("I am Quang Tho");
4     }
5
6     public static void main(String[] args) {
7       myMethod();
8     }
9   }
```

Cũng tại dòng 2 như mình đã nói chúng ta sẽ sử dụng từ khóa `void` và sau đó là tên của hàm và bên trong là những gì bạn muốn làm bên trong hàm này.

Ở dòng 7 chúng ta sẽ gọi lại hàm.

## Lưu ý

1. Khi xuất hiện từ khóa `return` trong hàm thì tất cả những dòng lệnh đằng sau `rerturn` sẽ không được thực hiện nữa

2. Khi bạn `return` cũng có nghĩa là bạn đang trả về giá trị của hàm đó

3. Khi bạn gọi lại hàm thì tất cả các lệnh trong hàm đó sẽ được thực hiện