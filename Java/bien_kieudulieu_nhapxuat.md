# Ở ĐÂY MÌNH SẼ KHÔNG DÀI DÒNG VÀ MÌNH SẼ ĐI VÀO VẤN ĐỀ LUÔN

## Cấu trúc của một trương trình Java cơ bản
**Hãy cùng theo dõi đoạn code dưới đây nhé**

`HelloWorld.java`
```
1   public class HelloWorld {
2       public static void main(String[] args) {
3           System.out.println("Hello World");    
4       }
5   }
```
```
Output: Hello World
```

**Ở trên là một cấu trúc chương trình Java cơ bản và mình sẽ giải thích từng dòng sau đây**

Dòng 1 là dòng khởi tạo `class` có tên là **HelloWorld**, `public` là từ khóa dùng để cấp quyền để `class` có thể được truy cập từ bên ngoài file **HelloWorld.java**.

Dòng 2 đây là hành động tạo 1 hàm có tên là `main` và hàm này được khai báo dưới dạng `void` (sẽ được học trong bài tiếp theo), `public` là hành động cấp phép cho chép truy cập đến hàm `main` từ bất cứ đâu, `static` các bạn sẽ được học ở các bài sau.
    
Dòng 3 `System.out.println("Hello World");` đây là hoạt động in ra màn hình.

_**Lưu ý: Đoạn code chúng ta vừa phân tích là đoạn code dùng để chạy chương trình có nó IDE mới có thể thực hiện trương trình của bạn bạn code ở bất cứ đâu nhưng cuối cùng tất cả phải được quy tụ vào hàm `main` được khai báo như trên với `public static void` thuộc 1 class bất kỳ được khai báo `public`**_

## Yeah các bạn đã hiểu được cấu trúc của một trương trình Java cơ bản và bây giờ là các khái niệm tiếp theo

# 1. Biến và kiểu dữ liệu
### Kiểu dữ liệu

| Data Type | Size |
| ------ | ------ |
| byte | 1 byte |
| short | 2 bytes |
| int |	4 bytes |
| long | 8 bytes |
| float | 4 bytes |
| double |	8 bytes |
| boolean |	1 bit |
| char | 2 bytes |
| String | |

## Biến: Bây giờ chúng ta sẽ học cách khai báo biến
### Các quy tắc đặt tên biến

- [x] Nó nên bắt đầu bằng một chữ cái viết thường như id, name.
- [x] Không nên bắt đầu bằng các ký tự đặc biệt như ký hiệu &, $ (đô la), _ (gạch dưới).
- [x] Nếu tên chứa nhiều từ, hãy bắt đầu bằng chữ cái viết thường theo sau là chữ cái viết hoa như firstName, lastName.
- [x] Tránh sử dụng các biến một ký tự như x, y, z

### Thông qua các kiểu dữ liệu đã có ở bên trên chúng ta sẽ khai báo biến cho từng kiểu dữ liệu như sau

**`byte, short, int, long` là các kiểu dữ liệu số nguyên chúng ta sẽ khai báo như sau:**
```
byte a = 3;
int b = 10;
short c = 20;
long d = 15000000000L;

```
_Lưu ý: với kiểu `long` chúng ta phải thêm 1 hậu tố là `L`_

**`float, double` là kiểu dữ liệu số thực sẽ được khai báo như sau**
```
float a = 1.5f;
double b = 2.6d;
double c = 2.9;
```
_Lưu ý: với kiểu `float` chúng ta cần thêm 1 hậu tố `f` và điểu này là bắt buộc còn đối với `double` bạn có thể thêm hoặc không thêm hậu tố `d`_

**`boolean` là kiểu giá trị logic nó sẽ có 2 giá trị mặc định là `true` hoặc `false` và được khai báo như sau**
```
boolean t = true;
boolean f = false;
```

**`char` là kiểu ký tự là gá trị của nó sẽ chỉ có 1 kí tự duy nhất và được đặt trong dấu nháy đơn `''`**
```
char a = 'a'; // đúng
char b = 'ab'; //sai
```

**`String` là kiểu dữ liệu bao gồm 1 dãy gồm nhiều ký tự và được đặt trong cặp dấu nháy đơn `""`**
```
String str = "Hello World";
```

# 2. Xuất và nhập dữ liệu ở trong Java

## Xuất dữ liệu mình đã nói ở trên đây là syntax `System.out.println(a);` với `a` là dữ liệu bạn cần in ra.

## Nhập dữ liệu: để có thể nhập được dữ liệu từ màn hình bạn cần sử dụng 1 pagkage có tên là `Scanner` và bạn cần đưa nó vào file mà bạn cần sử dụng như sau:

**`import java.util.Scanner;` lưu ý là dòng này phải được đặt ở đầu file.**

**Và để sử dụng nó bạn cần làm 2 bước:**

**Bạn phải tạo 1 đối tượng Scanner: `Scanner sc = new Scanner(System.in);`**

**Sẽ có một quy tắc cho việc sử dụng các đối tượng Scanner để nhập dữ liệu như sau.**

| câu lệnh | Kiểu dữ liệu |
| ------ | ------ |
| nextBoolean() | nhập dữ liệu kiểu `Boolean` |
| nextByte() | nhập dữ liệu kiểu `Byte` |
| nextDouble() | nhập dữ liệu kiểu `Double`|
| nextFloat() |	nhập dữ liệu kiểu `Float` |
| nextInt() | nhập dữ liệu kiểu `Integer` |
| nextLine() | nhập dữ liệu kiểu `String` |
|nextLong() | nhập dữ liệu kiểu `Long` |
| nextShort() | nhập dữ liệu kiểu `Short` |

Ví dụ:
`TestScanner.java`
```
1   import java.util.Scanner; // đưa đối tượng Scanner vào file
2
3   public class HelloWorld {
4       public static void main(String[] args) {
5           Scanner sc = new Scanner(System.in); // Tạo đối tượng Scanner để sử dụng
6           String name = sc.nextLine(); // sử dụng câu lệnh tương ứng với kiểu dữ liệu của biến
7           System.out.println(name);
8      }
9   }
```
```
Input: tho
Output: tho
```

# 3. Comments trong Java

## **Ở trên bạn đã thấy có những dòng code có hai dấu `//` rồi sau đó ghi một cái gì đó đằng sau. Vâng chúng ta gọi đó là Comments trong Java**

## Comments là những dòng được sử dụng để ghi chú một một thứ gì đó bạn muốn và trình biên dịch sẽ tự động bỏ qua và không thực thi nó

## **có 2 cách Comments đó là Comments 1 dòng là Comments nhiều dòng**

**Comments 1 dòng được bắt đầu từ đằng sau cặp dấu `//` và sau đó sẽ là những ghi chú mà các bạn cần thiết**

_ví dụ:_ `\\ đây là 1 dòng comment`

**Comments nhiều dòng các ghi chú sẽ được bọc ở trong 1 cặp ký tự như sau `/**/`**
_ví dụ:_ 
```
\* Đây là 1 ví dụ
về comment nhiều dòng
*\
```