# HÔM NAY MÌNH SẼ NÓI VỀ VÒNG LẶP MỘT VẤN ĐỀ KHÔNG THỂ THIẾU TRONG TẤT CẢ CÁC NGÔN NGỮ LẬP TRÌNH

## Vòng lặp for

**Ví dụ: In ra màn hình từ 1 đến 100**

```
1   public class MyClass {
2     public static void main(String[] args) {
3       for (int i = 1; i <= 100; i++) {
4         System.out.println(i);
5       }  
6     }
7   }
```

**Bắt đầu từ dòng thứ 3:**

Trong for chúng ta sẽ có 3 thứ. đầu tiên `int i = 0` là khởi tạo biến đếm và đặt giá trị ban đầu

Tiếp sau đó là `i <= 100` là điều kiện dừng của vòng lặp

và cuối cùng `i++` có chức năng tăng biến đếm `i` chứ không phải tự nhiên là nó tăng đâu nhé

## Vòng lặp While

**Ví dụ: In ra màn hình từ 1 đến 100**

```
1   public class MyClass {
2     public static void main(String[] args) {
3       int i = 1;
4       while (i <= 100) {
5           System.out.println(i);
6           i++;
7       }  
8     }
9   }
```

Đối với vòng lặp `while` chúng ta phải khai báo biến đếm bên ngoài vòng lặp trước ( dòng 3 )

Ở dòng 4 khi khai báo `while` chúng ta phải khai báo kèm theo điều kiện dừng của vòng lặp `i <= 100`

Ở dòng số 6 `i++` cũng có ý nghĩa là tăng giá trị biến đếm `i` chứ `i` không thể tự tăng được

## Vòng lặp do while

### Ở vòng lặp while vòng lặp sẽ kiểm tra điều kiện của biến đếm trước rồi mới lặp còn ở do while nó sẽ lặp trước rồi mới kiểm tra

**Ví dụ: In ra màn hình từ 1 đến 100**

```
1   public class MyClass {
2     public static void main(String[] args) {
3       int i = 1;
4       do {
5         System.out.println(i);
6         i++;
7       }
8       while (i <= 100);  
9     }
10  }
```

Dòng 3 vẫn như cũ chúng ta phải tạo biến đếm và gán giá trị ban đầu cho nó

Từ dòng 4 đế dòng 7 là bên trong `do` là nơi mà việc mình cần làm trong vòng lặp và `i++` vẫn có chức năng tăng giá trị biến đếm.

ở dòng thứ 8 mới là `while` và điều kiện dừng của nó `i <= 100`

**Có nhiều ông sẽ tự hỏi nhiều vòng lặp thế làm cái gì? Thì ở đây tôi trả lời luôn mỗi vòng lặp sẽ có các đặc tính khác nhau và có hữu ích trong 1 số công việc cụ thể khác nhau. Có những thứ `do while` làm được mà 2 cái kia không làm được. Nên tùy theo từng yêu cầu xử lý của vấn đề chúng ta sẽ sử dụng các vòng lặp khác nhau**
