# Bài này chúng ta sẽ nói đến toán tử điều kiện

## IF ELSE. Hãy cùng theo dõi ví dụ sau
**Ví dụ: Viết chương trình kiểm tra số chia hết cho 2 hay chia hết cho 3**

```
1    public class test {
2       public static void main(String[] args) {
3           int a = 13;
4           if (a % 2 == 0) {
5               System.out.print("a chia het cho 2");
6           }else if ( a % 3 == 0 ) {
7               System.out.println("a chia het cho 3");
8           }else {
9               System.out.println("chua xac dinh duoc");
10          }
11      }
12   }
```

### Hãy cùng nhau nhìn đoạn code trên

`if` bắt đầu từ dòng 4 là điều kiện đầu tiên ở đây nó sẽ kiểm tra điều kiện xem biến `a` có chia hết cho 2 hay không nếu có sẽ in ra `a chia het cho 2` nếu không sẽ bỏ qua và đến phần phía sau

Khi điều kiện ở dòng 4 sai thì nó sẽ tiếp tục nhảy xuống dòng 6 đây là điều kiện thứ 2 câu lệnh ở đây là `else if` nghĩa là một điều kiện khác và ở đây nó sẽ kiểm tra xem a có chia hết cho 3 hay không nếu có in ra `a chia hết cho 3` nếu không nó sẽ bỏ qua và đi tiếp

Khi điều kiện ở dòng 6 tiếp tục sai thì nó sẽ nhảy xuống dòng thứ 8 với câu lệnh là `else` nghĩa là tất cả các trường hợp trên không đúng thì ở đây sẽ thực thi

Từ dòng thứ 4 đến dòng thứ 10 chúng ta gọi nó là 1 cụm điều kiện

**Lưu ý: Khi 1 điều kiện trong cụm bắt đầu từ `if` và kết thúc ở `else` nếu 1 điều kiện nào đó đúng thì hiển nhiên những điều kiện bên dưới sẽ không được thực hiện**

## Switch case

**Hiển nhiên nếu có quá nhiều các dòng `if else` thì code nhìn sẽ thật xấu vậy nên để xử lý tình trạng đó chúng ta sẽ sử dụng `switch case` hãy cùng theo dõi ví dụ sau**

**_Ví dụ: Kiểm tra số được nhập vào có bằng 1, 2 hay 3 hay không_**

```
1    public class test {
2       public static void main(String[] args) {
3           int a = 4;
4           switch(a) {
5               case 1:
6                   System.out.print("a bang 1");
7                   break;
8               case 2:
9                   System.out.print("a bang 2");
10                  break;
11              case 3:
12                  System.out.print("a bang 3");
13                  break;
14              default:
15                  System.out.print("a khong bang cai nao");
16          }
17       }
18  }
```

Về logic thì nó cũng giống như `if else` `case` đầu tiên nếu `a` bằng 1 thì nó sẽ in ra `a bang 1` và không thì sẽ bỏ qua và thực hiện `case` tiếp theo và tương tự cho các `case` khác

`default` có chức năng giống như `else` nếu tất cả các `case` ở trên sai thì `default` sẽ được thực hiện

`break` là 1 từ khóa dùng để ngắt 1 `case` nào đó đúng và được thực thi hoàn tất nếu không có từ khóa `break` các case sau vẫn sẽ được chạy mặc dù có 1 `case` đã được thực thi
