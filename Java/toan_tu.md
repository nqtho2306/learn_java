# BÀI NÀY MÌNH SẼ NÓI ĐẾN TOÁN TỬ

## Toán tử số học
| Phép toán | Mô tả | Demo |
| ------ | ------ | ------ |
| + | Phép cộng | x + y  |
| - | Phép trừ | x - y |
| * | Phép nhân | x * y |
| / | Phép chia lấy phần nguyên | x / y |
| % | Phép chi lấy phần dư | x % y |

**Ví dụ:**
```
public class Operators {
    public static void main (String[] args) {
        int x = 6;
        int y = 7;
        x + y // 13 
        x - y // -1
        x * y // 42
        x / y // 1
        x % y // 6
    }
}
```

## Toán tử tăng và giảm

| toán tử | mô tả |
| ------ | ------ |
| ++ | Toán tử tăng | 
| -- | Toán tử giảm |

### Sẽ có 2 cách sử dụng toán tử trên 1 là đặt đằng trước biến 2 là đặt đằng sau biến:

```
++x;
x++;
```

### Đặt đằng trước và đằng sau là 2 câu chuyện hoàn toàn khác nhau

**Đặt đằng trước giá trị của biến sẽ được tăng ngay khi thực hiện toán tử**

```
int x = 10;
++x; // x = 11
```

**Đặt đằng sau giá trị của biến sẽ chỉ thay đổi khi gọi lại biến 1 lần nữa**

```
int x = 10;
x++; // x = 10
x; // x = 11
```

**Tương tự đối với `--`**

## Phép gán

| Toán tử | Ví dụ | Ý nghĩa |
| ------ |------ | ------- |
| = |	x = 5 |	x = 5 |	
| += | x += 3 |	x = x + 3 |	
| -= | x -= 3 |	x = x - 3 |	
| *= | x *= 3 |	x = x * 3 |	
| /= | x /= 3 |	x = x / 3 |	
| %= | x %= 3 |	x = x % 3 |

## Toán tử so sánh

| Toán tử | Mô tả | ví dụ |
| ------ | ------ | ------ |
| == | So sánh bằng | x == y |	
| != | So sánh không bằng | x != y |	
| > | So sánh lớn hơn | x > y |	
| < | So sánh nhỏ hơn | x < y |	
| >= | So sánh lớn hơn hoặc bằng | x >= y |	
| <= | So sánh nhỏ hơn hoặc bằng | x <= y |

## Toán tử Logic
| Toán tử | Tên | Mô tả | Ví dụ |
| ------ | ------ | ------ | ------ |
| && | Toán tử "VÀ" | Trả về True nếu cả 2 đúng |	x < 5 &&  x < 10 |
| \|\| | Toán tử "HOẶC" | Trả về True nếu 1 trong 2 đúng | x < 5 \|\| x < 4 |
| !	| Toán tử phủ định | Đảo từ True sang False và ngược lại | !(x < 5 && x < 10) |