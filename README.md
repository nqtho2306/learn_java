# Lời đầu tiên xin chào tất cả các bạn đang theo dõi những dòng này và chào mừng các bạn đến với chuỗi những bài viết về Java
## Đầu tiên mình xin giới thiệu một chút về bản thân mình

1. Mình tên là : Nguyễn Quang Thọ
2. Mình hiện là sinh viên khoa công nghệ thông tin trường đại học Nông Lâm Hồ Chí Minh
3. Mình đã có 2 năm kinh nghiệm trong việc làm mentor và làm các khóa học liên quan đến lập trình

## Và ở bài viết này mình sẽ nói về những kiến thức cơ bản nhất mà bất kỳ ai cũng cần phải nắm rõ để có thể làm việc với Java.
## Và dưới đây sẽ là link các bài viết liên quan

**1. [Biến, kiểu dữ liệu, nhập và xuất dữ liệu](https://gitlab.com/nqtho2306/learn_java/-/blob/master/Java/bien_kieudulieu_nhapxuat.md)**

**2. [Toán tử](https://gitlab.com/nqtho2306/learn_java/-/blob/master/Java/toan_tu.md)**

**3. [Câu lệnh điều kiện if else, switch case](https://gitlab.com/nqtho2306/learn_java/-/blob/master/Java/ifelse_switchcase.md)**

**4. [Vòng lặp for, while, do while](https://gitlab.com/nqtho2306/learn_java/-/blob/master/Java/vonglap.md)**

**5. [Hàm trong Java](https://gitlab.com/nqtho2306/learn_java/-/blob/master/Java/ham.md)**

(còn update)